#!/usr/bin/env python
import command

class Program:
    
    def __init__(self):
        self.commands = []
        
    def add_command(self, cmd):
        self.commands.append(cmd)
        
    def remove_command(self, cmd):
        self.commands.remove(cmd)
        
    def run_program(self):
        for cmd in self.commands:
            cmd.run()