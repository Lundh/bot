#!/usr/bin/env python

from command import Command

class Move_to(Command):
    
    def __init__(self, id):
        Command.__init__(self, id)
        self.endpoint = self.arm.get_current_pose()

        
    def run(self):
        self.arm.set_pose_target(self.endpoint)
        self.arm.set_goal_tolerance(0.3)
        plan = self.arm.go(wait=True)
        self.arm.stop()
        self.arm.clear_pose_targets()
        
        

