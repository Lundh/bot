#!/usr/bin/env python3

from kivy.uix.gridlayout import GridLayout
from kivy.app import App
from kivy.uix.button import Button
from layout import Layout

class MyApp(App):
    def build(self):
        layout = Layout(3, orientation='vertical')
        
        return layout

MyApp().run()