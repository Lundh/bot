#!/usr/bin/env python3

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button

class Layout(BoxLayout):
    
    def __init__(self, number_of_parts, **kwargs):
        super().__init__(**kwargs)
        