#!/usr/bin/env python

import rospy
import sys
sys.path.append("/home/hannes/catkin_ws/src/motion_scripts")
from controller.program import Program
from controller.move_to import Move_to
import moveit_commander
import geometry_msgs.msg



def main():
    rospy.init_node("test_program")
    prog = Program()
    cmd = Move_to(1)
    prog.add_command(cmd)
    move_group = moveit_commander.MoveGroupCommander("manipulator")
    move_group.stop()
    move_group.clear_pose_targets()
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.orientation.w = 1.0
    pose_goal.position.x = 0.4
    pose_goal.position.y = 0.1
    pose_goal.position.z = 0.4
    move_group.set_pose_target(pose_goal)
    move_group.set_goal_tolerance(0.01)
    move_group.plan(pose_goal)
    move_group.go(wait=True)
    move_group.stop()
    move_group.clear_pose_targets()
    
    
    
#    prog.run_program()

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        print ("Program interrupted before completion")
    
    