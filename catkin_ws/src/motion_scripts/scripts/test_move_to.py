#!/usr/bin/env python

import rospy, sys
import moveit_commander
from geometry_msgs.msg import Pose
from copy import deepcopy
from std_msgs.msg import Header

from trajectory_msgs.msg import JointTrajectory

from trajectory_msgs.msg import JointTrajectoryPoint


endpoints = [[0.0, -1, 1.5, 1.0, 0, -1]]

def main():

    rospy.init_node('test_move')
    pub = rospy.Publisher('/arm_controller/command',
                          JointTrajectory,
                          queue_size=10)


    arm = moveit_commander.MoveGroupCommander('manipulator')
    endpoints.append(arm.get_current_joint_values())

    # Create the topic message
    traj = JointTrajectory()
    traj.header = Header()
    # Joint names for UR5
    traj.joint_names = ['shoulder_pan_joint', 'shoulder_lift_joint',
                        'elbow_joint', 'wrist_1_joint', 'wrist_2_joint',
                        'wrist_3_joint']

    rate = rospy.Rate(1)
    move_away_from_all_zeros = True
    pts = JointTrajectoryPoint()
    traj.header.stamp = rospy.Time.now()

    while not rospy.is_shutdown():

        if  move_away_from_all_zeros:
            pts.positions = endpoints[0]
            move_away_from_all_zeros = False
        else:
            pts.positions = endpoints[1]
            move_away_from_all_zeros = True

        pts.time_from_start = rospy.Duration(1.0)

        # Set the points to the trajectory
        traj.points = []
        traj.points.append(pts)
        # Publish the message
        pub.publish(traj)
        rate.sleep()

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        print ("Program interrupted before completion")